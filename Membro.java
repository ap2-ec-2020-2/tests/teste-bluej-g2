public class Membro{
    
    private String nome;
    private String sobrenome;
    private int semestre;
 
    
    
    
    public Membro(String nome, String sobrenome, int semestre)
    {   
  
        this.nome = nome;
        this.sobrenome = sobrenome;
        this.semestre = semestre;
    }
    
    public String getNomeCompleto()
    {
        return nome.concat(sobrenome);
    }
    
    public int getSemestre()
    {
        return this.semestre;
    }
}